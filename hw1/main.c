#include <stdio.h>

void triR(void) {
	int size, repeat;
	scanf("%d %d", &size, &repeat);//숫자,  반복횟수
	printf("Hello world\n");

// 이 함수를 완성하시오. (4점)

    int n, a, b;
     if(size == 0) {}
	 else
	 {
		for(n = 1; n <= repeat; n++) // 소스코드 반복
		{
			printf("\n");
			for(a = 1; a <= size ; a++)
			{
				for(b = 1; b <= a; b++) // 숫자 반복
				{
					printf("%d", a);
				}
				printf("\n");
			}
			// 여기까지 증가 아래부터는 감소

 	  		for(a = size-1; a > 0; a--)
	    	{
				for(b = 1; b <= a; b++) // 숫자 반복
				{
					printf("%d", a);
				}
				printf("\n");
			}
		}
	}
		printf("Bye world\n");
}

void triL(void){

	int size, repeat;

	scanf("%d %d", &size, &repeat);
	printf ("Hello world\n");
  // 이 함수를 완성하시오. (1점)

    int n, a, b;
     if(size == 0) {}
	 else
	 {
		for(n = 1; n <= repeat; n++) // 소스코드 반복
		{
			for(b = 1; b <= size; b++) // 온점 출력
			{
				printf(".");
			}
			printf("\n");

			for(a = 1; a <= size ; a++)
			{
				for(b = 1; b <= size - a; b++) // 온점 출력
				{
					printf(".");
				}
				for(b = 1; b <= a; b++) // 숫자 반복
				{
					printf("%d", a);
				}
				printf("\n");
			}
			// 여기까지 증가 아래부터는 감소

 	  		for(a = size-1; a > 0; a--)
	    	{
	    		for(b = 1; b <= size - a; b++) // 온점 출력
				{
					printf(".");
				}
				for(b = 1; b <= a; b++) // 숫자 반복
				{
					printf("%d", a);
				}
				printf("\n");
			}
		}
	}
		printf("Bye world\n");
}

void dias(void){

	int size, repeat;

	scanf("%d %d", &size, &repeat);
	printf("Hello world\n");

	// 이 함수를 완성하시오 (1점)
	// ...

    int n, a, b;
     if(size == 0) {}
	 else
	 {
		for(n = 1; n <= repeat; n++) // 소스코드 반복
		{
			for(b = 1; b <= size; b++) // 온점 출력
			{
				printf(".");
			}
			printf("\n");

			for(a = 1; a <= size ; a++)
			{
				for(b = 1; b <= size - a; b++) // 온점 출력
				{
					printf(".");
				}
				for(b = 1; b <= a*2; b++) // 숫자 반복
				{
					printf("%d", a);
				}
				printf("\n");
			}
			// 여기까지 증가 아래부터는 감소

 	  		for(a = size-1; a > 0; a--)
	    	{
	    		for(b = 1; b <= size - a; b++) // 온점 출력
				{
					printf(".");
				}
				for(b = 1; b <= a*2; b++) // 숫자 반복
				{
					printf("%d", a);
				}
				printf("\n");
			}
		}
	}
		printf("Bye world\n");
}

int main(void)
{
  int n;
  scanf("%d", &n); // 1,2,3 중 하나를 입력받는다
  switch (n)
  {
    case 1: triR(); break;
    case 2: triL(); break;
    case 3: dias(); break;
    default: return -1;
  }
  return 0;
}
